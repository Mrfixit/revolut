package com.dbel.revolutviewer.converter.usecases

import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import com.dbel.revolutviewer.domain.repo.CurrencyRatesRepository
import com.dbel.revolutviewer.domain.usecases.GetRatesForCurrencyEachSecondUseCase
import com.dbel.revolutviewer.domain.usecases.GetRatesForCurrencyEachSecondUseCaseImpl
import com.dbel.revolutviewer.utils.schedulers.SchedulerManager
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

class GetRatesForCurrencyUseCaseTests {

    @Mock
    lateinit var schedulerManager: SchedulerManager

    @Mock
    lateinit var currencyRepository: CurrencyRatesRepository

    private lateinit var useCase: GetRatesForCurrencyEachSecondUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        whenever(schedulerManager.getIoScheduler()).thenReturn(Schedulers.trampoline())
        whenever(schedulerManager.getMainScheduler()).thenReturn(Schedulers.trampoline())
        useCase = GetRatesForCurrencyEachSecondUseCaseImpl(schedulerManager, currencyRepository)
    }

    @Test
    fun useCase_shouldCallRepository_and_get_item() {
        //GIVEN
        val ratesMap = mutableMapOf<String, BigDecimal>()
        val currencies = Currency.getAvailableCurrencies().toList()

        for (i in currencies.indices) {
            ratesMap[currencies[i].currencyCode] = BigDecimal(Random().nextInt(100))
        }
        val baseCurrency = currencies[0].currencyCode
        val expectedResult = CurrencyRatesDataLo(baseCurrency, ratesMap)

        whenever(currencyRepository.getLatestRates(baseCurrency)).thenReturn(
            Flowable.just(
                expectedResult
            )
        )

        //WHEN
        val testObserver = useCase.execute(baseCurrency).test()
        testObserver.awaitCount(1)

        //THEN
        verify(currencyRepository, times(1)).getLatestRates(baseCurrency)
        testObserver.assertNoErrors()
            .assertValue { it == expectedResult }
    }

    @Test
    fun useCase_shouldCallRepository_and_get_2_items_for_2_seconds() {
        //GIVEN
        val ratesMap = mutableMapOf<String, BigDecimal>()
        val currencies = Currency.getAvailableCurrencies().toList()

        for (i in currencies.indices) {
            ratesMap[currencies[i].currencyCode] = BigDecimal(Random().nextInt(100))
        }
        val baseCurrency = currencies[0].currencyCode
        val expectedResult = CurrencyRatesDataLo(baseCurrency, ratesMap)

        whenever(currencyRepository.getLatestRates(baseCurrency)).thenReturn(
            Flowable.just(
                expectedResult
            )
        )

        //WHEN
        val testObserver = useCase.execute(baseCurrency).test()
        testObserver.awaitCount(2, {}, TimeUnit.SECONDS.toMillis(2))

        //THEN
        verify(currencyRepository, times(2)).getLatestRates(baseCurrency)
        testObserver.assertNoErrors()

    }


}