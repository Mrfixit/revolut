package com.dbel.revolutviewer.converter.presentation

import com.dbel.revolutviewer.converter.ConverterTestUtils
import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import com.dbel.revolutviewer.domain.usecases.GetRatesForCurrencyEachSecondUseCase
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import com.dbel.revolutviewer.presentation.converter.mvp.ConverterPresenter
import com.dbel.revolutviewer.presentation.converter.mvp.ConverterView
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.math.BigDecimal

class ConverterPresenterTests {

    @Mock
    lateinit var view: ConverterView

    @Mock
    lateinit var getRatesUseCase: GetRatesForCurrencyEachSecondUseCase

    lateinit var presenter: ConverterPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = ConverterPresenter(getRatesUseCase)
        presenter.attachView(view)
    }

    @Test
    fun converterPresenter_should_have_eur_default_value() {
        whenever(getRatesUseCase.execute(any())).thenReturn(
            Flowable.just(
                CurrencyRatesDataLo(
                    "",
                    mapOf()
                )
            )
        )
        presenter.onFirstInit()
        verify(getRatesUseCase).execute(argWhere { it == "EUR" })

        println("converterPresenter_should_have_eur_default_value PASSED")
    }

    @Test
    fun converterPresenter_should_show_loading_and_hide_content_on_firstInit() {
        whenever(getRatesUseCase.execute(any())).thenReturn(
            Flowable.just(
                CurrencyRatesDataLo(
                    "",
                    mapOf()
                )
            )
        )

        presenter.onFirstInit()

        verify(view, times(1)).hideContent()
        verify(view, times(1)).showLoading()

        println("converterPresenter_should_show_loading_and_hide_content_on_firstInit PASSED")
    }

    @Test
    fun converterPresenter_should_subscribe_to_data_on_restore() {

        whenever(getRatesUseCase.execute(any())).thenReturn(
            Flowable.just(
                CurrencyRatesDataLo(
                    "",
                    mapOf()
                )
            )
        )

        presenter.onRestoreData()

        verify(getRatesUseCase, times(1)).execute(any())

        println("converterPresenter_should_subscribe_to_data_on_restore PASSED")
    }


    @Test
    fun converterPresenter_should_change_base_currency_and_refreshItems_onClick() {
        //GIVEN
        val defaultCurrency = ConverterTestUtils.getDefaultCurrency()
        val defaultAmount = ConverterTestUtils.getDefaultAmount()
        val defaultRates = ConverterTestUtils.createDefaultRatesMap()

        val defaultRawRates =
            ConverterTestUtils.createDefaultRatesData(defaultCurrency, defaultRates)

        val defaultViewRates =
            ConverterTestUtils.createDefaultCurrencyExchangeList(
                defaultCurrency,
                defaultAmount,
                defaultRates
            )

        val newBaseCurrency = "RUB"
        val newRawRates = CurrencyRatesDataLo(
            newBaseCurrency,
            mapOf(
                Pair("EUR", BigDecimal(0.014)),
                Pair("USD", BigDecimal(0.9)),
                Pair("GBP", BigDecimal(1.1))
            )
        )
        val newExpectedResult = listOf(
            CurrencyExchangeItem(newBaseCurrency, BigDecimal(7000), true),
            CurrencyExchangeItem("EUR", BigDecimal(100), false),
            CurrencyExchangeItem("USD", BigDecimal(110), false),
            CurrencyExchangeItem("GBP", BigDecimal(90), false)
        )

        whenever(getRatesUseCase.execute(defaultCurrency)).thenReturn(Flowable.just(defaultRawRates))
        whenever(getRatesUseCase.execute(newBaseCurrency)).thenReturn(Flowable.just(newRawRates))

        //THEN

        //Init default currency
        presenter.onBaseItemAmountChange(defaultViewRates[0])
        presenter.onFirstInit()
        verify(view).setRates(argWhere { it == defaultViewRates })

        // Change baseItem
        presenter.onRateItemClick(
            CurrencyExchangeItem(
                newBaseCurrency,
                defaultAmount.multiply(defaultRates[newBaseCurrency])
            )
        )
        verify(view, times(2)).refreshRates(argWhere { it == newExpectedResult })

        println("converterPresenter_should_change_base_currency_and_refreshItems_onClick PASSED")
    }

}