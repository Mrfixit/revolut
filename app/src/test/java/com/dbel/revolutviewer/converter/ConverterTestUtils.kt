package com.dbel.revolutviewer.converter

import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import java.math.BigDecimal

object ConverterTestUtils {


    internal fun createDefaultCurrencyExchangeList(
        baseCurrency: String,
        baseAmount: BigDecimal,
        rates: Map<String, BigDecimal>
    ): List<CurrencyExchangeItem> {
        return listOf(
            CurrencyExchangeItem(baseCurrency, baseAmount, true),
            CurrencyExchangeItem("RUB", baseAmount.multiply(rates["RUB"]), false),
            CurrencyExchangeItem("USD", baseAmount.multiply(rates["USD"]), false),
            CurrencyExchangeItem("GBP", baseAmount.multiply(rates["GBP"]), false)
        )
    }

    internal fun createDefaultRatesMap(): Map<String, BigDecimal> {
        return mapOf(
            Pair("RUB", BigDecimal(70)),
            Pair("USD", BigDecimal(1.1)),
            Pair("GBP", BigDecimal(0.9))
        )
    }

    internal fun createDefaultRatesData(
        defaultCurrency: String,
        rates: Map<String, BigDecimal>
    ): CurrencyRatesDataLo {
        return CurrencyRatesDataLo(defaultCurrency, rates)
    }

    internal fun getDefaultCurrency(): String = "EUR"

    internal fun getDefaultAmount(): BigDecimal = BigDecimal(100)

}