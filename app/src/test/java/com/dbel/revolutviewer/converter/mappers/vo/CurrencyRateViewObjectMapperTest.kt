package com.dbel.revolutviewer.converter.mappers.vo

import com.dbel.revolutviewer.converter.ConverterTestUtils
import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import com.dbel.revolutviewer.presentation.converter.model.toAdapterItems
import org.junit.Before
import org.junit.Test

class CurrencyRateViewObjectMapperTest {

    @Before
    fun setUp() {
    }

    @Test
    fun currencyDto_to_lo() {
        val baseInputAmount = ConverterTestUtils.getDefaultAmount()
        val baseCurrency = ConverterTestUtils.getDefaultCurrency()
        val baseRates = ConverterTestUtils.createDefaultRatesMap()
        val lo = CurrencyRatesDataLo(baseCurrency, baseRates)

        val expectedResults = mutableListOf<CurrencyExchangeItem>()

        for (item in lo.rates) {
            expectedResults.add(
                CurrencyExchangeItem(
                    item.key,
                    item.value.multiply(baseInputAmount)
                )
            )
        }

        val actualResults = lo.toAdapterItems(baseInputAmount)

        assert(actualResults.size == expectedResults.size)

        for (i in actualResults.indices) {
            val actual = actualResults[i]
            val expected = expectedResults[i]
            assert(actual.currencyCode == expected.currencyCode && actual.value.compareTo(expected.value) == 0)
        }

    }


}