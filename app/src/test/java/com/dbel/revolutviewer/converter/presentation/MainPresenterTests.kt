package com.dbel.revolutviewer.converter.presentation

import com.dbel.revolutviewer.presentation.main.mvp.MainPresenter
import com.dbel.revolutviewer.presentation.main.navigation.MainNavigator
import com.nhaarman.mockito_kotlin.argWhere
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import ru.terrakok.cicerone.Router

class MainPresenterTests {
    @Mock
    lateinit var router: Router

    private lateinit var presenter: MainPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MainPresenter(router)
    }


    @Test
    fun mainPresenter_shouldCreate_converterRootScreen() {
        presenter.onFirstInit()
        verify(
            router,
            times(1)
        ).newRootScreen(argWhere { it is MainNavigator.Screens.ConverterScreen })
    }
}