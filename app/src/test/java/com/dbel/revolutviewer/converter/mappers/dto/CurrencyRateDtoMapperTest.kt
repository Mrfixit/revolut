package com.dbel.revolutviewer.converter.mappers.dto

import com.dbel.revolutviewer.converter.ConverterTestUtils
import com.dbel.revolutviewer.data.models.currencies.CurrencyRatesDataDto
import com.dbel.revolutviewer.data.models.currencies.toDomain
import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import org.junit.Before
import org.junit.Test
import org.threeten.bp.LocalDate

class CurrencyRateDtoMapperTest {

    @Before
    fun setUp() {
    }

    @Test
    fun currencyDto_to_lo() {
        val baseCurrency = ConverterTestUtils.getDefaultCurrency()
        val baseRates = ConverterTestUtils.createDefaultRatesMap()

        val dto = CurrencyRatesDataDto(baseCurrency, LocalDate.MAX, baseRates)

        val expectedResult = CurrencyRatesDataLo(baseCurrency, baseRates)

        assert(dto.toDomain() == expectedResult)

    }


}