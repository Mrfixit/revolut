package com.dbel.revolutviewer.domain.repo

import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import io.reactivex.Flowable

interface CurrencyRatesRepository {
    fun getLatestRates(base: String): Flowable<CurrencyRatesDataLo>
}