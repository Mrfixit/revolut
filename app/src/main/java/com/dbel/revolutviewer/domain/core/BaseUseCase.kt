package com.dbel.revolutviewer.domain.core

import com.dbel.revolutviewer.utils.schedulers.SchedulerManager
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

open class BaseUseCase(
    val schedulerManager: SchedulerManager
) {

    fun <T> Flowable<T>.applySchedulers(): Flowable<T> = this
        .subscribeOn(schedulerManager.getIoScheduler())
        .observeOn(schedulerManager.getMainScheduler())

    fun <T> Observable<T>.applySchedulers(): Observable<T> = this
        .subscribeOn(schedulerManager.getIoScheduler())
        .observeOn(schedulerManager.getMainScheduler())

    fun <T> Single<T>.applySchedulers(): Single<T> = this
        .subscribeOn(schedulerManager.getIoScheduler())
        .observeOn(schedulerManager.getMainScheduler())

    fun Completable.applySchedulers(): Completable = this
        .subscribeOn(schedulerManager.getIoScheduler())
        .observeOn(schedulerManager.getMainScheduler())

}