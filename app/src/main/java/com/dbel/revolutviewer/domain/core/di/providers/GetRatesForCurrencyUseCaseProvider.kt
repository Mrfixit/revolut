package com.dbel.revolutviewer.domain.core.di.providers

import com.dbel.revolutviewer.domain.repo.CurrencyRatesRepository
import com.dbel.revolutviewer.domain.usecases.GetRatesForCurrencyEachSecondUseCase
import com.dbel.revolutviewer.domain.usecases.GetRatesForCurrencyEachSecondUseCaseImpl
import com.dbel.revolutviewer.utils.schedulers.SchedulerManager
import javax.inject.Inject
import javax.inject.Provider


class GetRatesForCurrencyUseCaseProvider @Inject constructor(
    private val schedulerManager: SchedulerManager,
    private val repository: CurrencyRatesRepository
) : Provider<GetRatesForCurrencyEachSecondUseCase> {
    override fun get(): GetRatesForCurrencyEachSecondUseCase {
        return GetRatesForCurrencyEachSecondUseCaseImpl(
            schedulerManager,
            repository
        )
    }
}