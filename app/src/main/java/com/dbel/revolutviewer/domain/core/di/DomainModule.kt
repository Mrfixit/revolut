package com.dbel.revolutviewer.domain.core.di

import com.dbel.revolutviewer.domain.core.di.providers.GetRatesForCurrencyUseCaseProvider
import com.dbel.revolutviewer.domain.usecases.GetRatesForCurrencyEachSecondUseCase
import toothpick.config.Module

class DomainModule : Module() {
    init {
        bind(GetRatesForCurrencyEachSecondUseCase::class.java)
            .toProvider(GetRatesForCurrencyUseCaseProvider::class.java)
            .providesSingleton()
    }
}