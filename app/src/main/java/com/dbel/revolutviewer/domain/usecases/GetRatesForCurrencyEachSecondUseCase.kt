package com.dbel.revolutviewer.domain.usecases

import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import io.reactivex.Flowable

interface GetRatesForCurrencyEachSecondUseCase {
    fun execute(baseCurrency: String): Flowable<CurrencyRatesDataLo>
}