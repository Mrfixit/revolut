package com.dbel.revolutviewer.domain.usecases

import com.dbel.revolutviewer.domain.core.BaseUseCase
import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import com.dbel.revolutviewer.domain.repo.CurrencyRatesRepository
import com.dbel.revolutviewer.utils.schedulers.SchedulerManager
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class GetRatesForCurrencyEachSecondUseCaseImpl(
    schedulerManager: SchedulerManager,
    private val repo: CurrencyRatesRepository
) : GetRatesForCurrencyEachSecondUseCase, BaseUseCase(schedulerManager) {
    override fun execute(baseCurrency: String): Flowable<CurrencyRatesDataLo> {
        return Flowable.interval(0, 1, TimeUnit.SECONDS)
            .flatMap { repo.getLatestRates(baseCurrency) }
            .applySchedulers()

    }
}