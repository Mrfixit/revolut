package com.dbel.revolutviewer.domain.models

import java.math.BigDecimal

data class CurrencyRatesDataLo(

    val baseCurrencyName: String,

    val rates: Map<String, BigDecimal>

) : BaseLo()