package com.dbel.revolutviewer.data.core.network.api.revolut

import com.dbel.revolutviewer.data.models.currencies.CurrencyRatesDataDto
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

    @GET("/latest")
    fun getLatestRates(@Query("base") baseCurrency: String): Flowable<CurrencyRatesDataDto>

}