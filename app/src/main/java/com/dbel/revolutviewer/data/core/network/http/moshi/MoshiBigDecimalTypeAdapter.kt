/*
 * Copyright © 2017. Vivira Health Lab. All rights reserved.
 */

package com.dbel.revolutviewer.data.core.network.http.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.math.BigDecimal

class MoshiBigDecimalTypeAdapter {

    @FromJson
    fun localDateFromJson(value: Double): BigDecimal = BigDecimal(value)

    @ToJson
    fun localDateToJson(value: BigDecimal): Double = value.toDouble()

}
