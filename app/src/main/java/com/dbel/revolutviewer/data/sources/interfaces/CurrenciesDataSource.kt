package com.dbel.revolutviewer.data.sources.interfaces

import com.dbel.revolutviewer.data.models.currencies.CurrencyRatesDataDto
import io.reactivex.Flowable

interface CurrenciesDataSource {
    fun getLatestRates(base: String): Flowable<CurrencyRatesDataDto>
}