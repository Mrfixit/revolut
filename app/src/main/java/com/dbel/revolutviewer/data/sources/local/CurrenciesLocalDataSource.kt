package com.dbel.revolutviewer.data.sources.local

import com.dbel.revolutviewer.data.models.currencies.CurrencyRatesDataDto
import com.dbel.revolutviewer.data.sources.interfaces.CurrenciesDataSource
import io.reactivex.Flowable

class CurrenciesLocalDataSource(
) : CurrenciesDataSource {

    override fun getLatestRates(base: String): Flowable<CurrencyRatesDataDto> {
        throw NoSuchMethodError("Local getLatestRates does not exist")
    }

}