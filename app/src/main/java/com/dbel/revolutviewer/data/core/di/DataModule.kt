package com.dbel.revolutviewer.data.core.di

import com.dbel.revolutviewer.data.core.di.providers.CurrenciesRepositoryProvider
import com.dbel.revolutviewer.data.core.network.api.revolut.RevolutApi
import com.dbel.revolutviewer.data.core.network.api.revolut.RevolutApiProvider
import com.dbel.revolutviewer.data.core.network.http.moshi.MoshiProvider
import com.dbel.revolutviewer.data.core.network.http.ok.OkHttpClientProvider
import com.dbel.revolutviewer.data.core.network.http.retrofit.RetrofitClientProvider
import com.dbel.revolutviewer.domain.repo.CurrencyRatesRepository
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import toothpick.config.Module

class DataModule : Module() {
    init {
        bindRemote()
        bindApi()
        bindRepos()
    }


    private fun bindRemote() {
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java)
            .providesSingleton()

        bind(Moshi::class.java).toProvider(MoshiProvider::class.java).providesSingleton()

        bind(Retrofit::class.java).toProvider(RetrofitClientProvider::class.java)
            .providesSingleton()

    }

    private fun bindApi() {
        bind(RevolutApi::class.java).toProvider(RevolutApiProvider::class.java)
            .providesSingleton()
    }

    private fun bindRepos() {
        bind(CurrencyRatesRepository::class.java).toProvider(CurrenciesRepositoryProvider::class.java)
            .providesSingleton()
    }

}
