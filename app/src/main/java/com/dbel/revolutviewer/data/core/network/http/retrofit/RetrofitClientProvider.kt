package com.dbel.revolutviewer.data.core.network.http.retrofit

import com.dbel.revolutviewer.data.core.network.Endpoints
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class RetrofitClientProvider @Inject constructor(
    val moshi: Moshi
) : Provider<Retrofit> {

    override fun get(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(Endpoints.ENDPOINT_REVOLUT)
            .build()
    }


}