package com.dbel.revolutviewer.data.core.network.http.moshi

import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Provider

class MoshiProvider @Inject constructor() : Provider<Moshi> {
    override fun get(): Moshi {
        return Moshi.Builder()
            .add(MoshiBigDecimalTypeAdapter())
            .add(MoshiLocalDateTypeAdapter())
            .build()
    }
}