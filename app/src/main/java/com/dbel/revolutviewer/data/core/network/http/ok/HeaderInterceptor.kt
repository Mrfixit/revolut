package com.dbel.revolutviewer.data.core.network.http.ok

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class HeaderInterceptor() : Interceptor {


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val requestBuilder = original.newBuilder()

        for ((key, value) in getHeaders()) {
            requestBuilder.header(key, value)
        }

        return chain.proceed(requestBuilder.build())
    }


    private fun getHeaders(): Map<String, String> {
        return mutableMapOf()
    }

    companion object {
        //names
    }
}