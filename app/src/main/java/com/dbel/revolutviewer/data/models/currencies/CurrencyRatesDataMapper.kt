package com.dbel.revolutviewer.data.models.currencies

import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo

fun CurrencyRatesDataDto.toDomain(): CurrencyRatesDataLo =
    CurrencyRatesDataLo(this.base, this.rates)