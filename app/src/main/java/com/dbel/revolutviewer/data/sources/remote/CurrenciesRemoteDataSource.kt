package com.dbel.revolutviewer.data.sources.remote

import com.dbel.revolutviewer.data.core.network.api.revolut.RevolutApi
import com.dbel.revolutviewer.data.models.currencies.CurrencyRatesDataDto
import com.dbel.revolutviewer.data.sources.interfaces.CurrenciesDataSource
import io.reactivex.Flowable

class CurrenciesRemoteDataSource(
    private val revolutApi: RevolutApi
) : CurrenciesDataSource {

    override fun getLatestRates(base: String): Flowable<CurrencyRatesDataDto> {
        return revolutApi.getLatestRates(base)
    }

}