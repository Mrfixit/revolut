package com.dbel.revolutviewer.data.models.currencies

import com.dbel.revolutviewer.data.models.BaseDto
import org.threeten.bp.LocalDate
import java.math.BigDecimal

data class CurrencyRatesDataDto(

    val base: String,

    val date: LocalDate,

    val rates: Map<String, BigDecimal>

) : BaseDto() {

}