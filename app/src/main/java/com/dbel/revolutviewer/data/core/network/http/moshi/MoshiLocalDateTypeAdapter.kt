/*
 * Copyright © 2017. Vivira Health Lab. All rights reserved.
 */

package com.dbel.revolutviewer.data.core.network.http.moshi

import com.dbel.revolutviewer.utils.DateTimeConverterUtils
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.threeten.bp.LocalDate

class MoshiLocalDateTypeAdapter {

    @FromJson
    fun localDateFromJson(localDate: String): LocalDate =
        DateTimeConverterUtils.asLocalDate(localDate)

    @ToJson
    fun localDateToJson(localDate: LocalDate): String = DateTimeConverterUtils.asString(localDate)

}
