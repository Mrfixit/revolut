package com.dbel.revolutviewer.data.repo

import com.dbel.revolutviewer.data.models.currencies.toDomain
import com.dbel.revolutviewer.data.sources.interfaces.CurrenciesDataSource
import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import com.dbel.revolutviewer.domain.repo.CurrencyRatesRepository
import io.reactivex.Flowable
import javax.inject.Inject

class CurrencyRatesRepositoryImpl @Inject constructor(
    private val localDataSource: CurrenciesDataSource,
    private val remoteDataSource: CurrenciesDataSource
) : CurrencyRatesRepository {

    override fun getLatestRates(base: String): Flowable<CurrencyRatesDataLo> {
        return remoteDataSource.getLatestRates(base)
            .map { it.toDomain() }
    }

}