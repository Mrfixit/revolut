package com.dbel.revolutviewer.data.core.di.providers

import com.dbel.revolutviewer.data.core.network.api.revolut.RevolutApi
import com.dbel.revolutviewer.data.repo.CurrencyRatesRepositoryImpl
import com.dbel.revolutviewer.data.sources.local.CurrenciesLocalDataSource
import com.dbel.revolutviewer.data.sources.remote.CurrenciesRemoteDataSource
import com.dbel.revolutviewer.domain.repo.CurrencyRatesRepository
import javax.inject.Inject
import javax.inject.Provider

class CurrenciesRepositoryProvider @Inject constructor(
    private val revolutApi: RevolutApi
) : Provider<CurrencyRatesRepository> {
    override fun get(): CurrencyRatesRepository {
        return CurrencyRatesRepositoryImpl(
            CurrenciesLocalDataSource(),
            CurrenciesRemoteDataSource(revolutApi)
        )
    }
}