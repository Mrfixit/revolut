package com.dbel.revolutviewer.data.core.network.api.revolut

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Provider


class RevolutApiProvider @Inject constructor(
    private val retrofit: Retrofit
) : Provider<RevolutApi> {

    override fun get(): RevolutApi {
        return retrofit.create(RevolutApi::class.java)
    }

}