package com.dbel.revolutviewer.presentation.converter.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dbel.revolutviewer.R
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import com.dbel.revolutviewer.presentation.core.ui.adapter.AdapterItem
import com.dbel.revolutviewer.presentation.core.ui.adapter.AdapterViewHolder
import com.dbel.revolutviewer.utils.inflate
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.mynameismidori.currencypicker.ExtendedCurrency
import kotlinx.android.synthetic.main.delegate_converter_currency_row.view.*
import java.math.BigDecimal
import java.math.RoundingMode


class ConverterCurrencyRowDelegate(
    private val clickListener: (item: CurrencyExchangeItem) -> Unit,
    private val textChangeListener: (item: CurrencyExchangeItem) -> Unit
) : AdapterDelegate<List<AdapterItem>>() {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.delegate_converter_currency_row)
        return ViewHolder(
            view,
            clickListener,
            ConverterAmountWatcher(textChangeListener)
        )
    }

    override fun isForViewType(items: List<AdapterItem>, position: Int): Boolean {
        return items[position] is CurrencyExchangeItem
    }

    override fun onBindViewHolder(
        items: List<AdapterItem>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder as ViewHolder
        val item = items[position] as CurrencyExchangeItem

        if (payloads.isNotEmpty() && payloads.last() is BigDecimal) {
            item.value = payloads.last() as BigDecimal
        }

        holder.bind(item)
    }


    companion object {
        private class ViewHolder(
            view: View,
            private val clickListener: (item: CurrencyExchangeItem) -> Unit,
            private val amountWatcher: ConverterAmountWatcher
        ) : AdapterViewHolder<CurrencyExchangeItem>(view) {
            override fun bind(item: CurrencyExchangeItem, position: Int) {
                bindCurrencyData(item, itemView)
                bindCurrencyAmount(item, itemView)
            }


            private fun bindCurrencyAmount(
                item: CurrencyExchangeItem,
                itemView: View
            ) {

                val exchangeValue = if (item.value.compareTo(BigDecimal.ZERO) != 0) {
                    item.value.setScale(2, RoundingMode.HALF_EVEN)
                        .stripTrailingZeros()
                        .toPlainString()
                } else {
                    ""
                }

                amountWatcher.item = item // set item for watcher for every item

                itemView.apply {
                    setOnClickListener { if (!item.isBaseCurrency) clickListener(item) }
                    edit_currency_value.apply {
                        isEnabled = item.isBaseCurrency
                        removeTextChangedListener(amountWatcher)
                        setText(exchangeValue)
                        setSelection(text.length)
                        addTextChangedListener(amountWatcher)
                    }
                }
            }

            private fun bindCurrencyData(item: CurrencyExchangeItem, itemView: View) {
                val currency = ExtendedCurrency.getCurrencyByISO(item.currencyCode)
                if (currency != null) {
                    itemView.apply {
                        text_currency_code.text = currency.code
                        text_currency_name.text = currency.name
                        Glide.with(context)
                            .load(currency.flag)
                            .apply(RequestOptions.circleCropTransform())
                            .into(image_currency_flag)
                    }
                } else {
                    itemView.apply {
                        text_currency_code.text = item.currencyCode
                        text_currency_name.text = item.currencyCode
                        Glide.with(context)
                            .load(R.drawable.flag_eur)
                            .apply(RequestOptions.circleCropTransform())
                            .into(image_currency_flag)
                    }
                }
            }

        }
    }
}