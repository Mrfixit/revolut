package com.dbel.revolutviewer.presentation.converter.di

import com.dbel.revolutviewer.presentation.converter.ui.ConverterFragment
import toothpick.config.Module

class ConverterModule(fragment: ConverterFragment) : Module() {
    init {
        bind(ConverterFragment::class.java).toInstance(fragment)
    }
}