package com.dbel.revolutviewer.presentation.converter.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ConverterScope {
}