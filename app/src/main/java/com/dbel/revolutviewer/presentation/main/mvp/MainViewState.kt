package com.dbel.revolutviewer.presentation.main.mvp

import android.os.Bundle
import com.dbel.revolutviewer.presentation.core.mvp.BaseViewState
import com.dbel.revolutviewer.presentation.main.di.MainScope
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState
import javax.inject.Inject

@MainScope
class MainViewState @Inject constructor() : BaseViewState<MainView>() {
    override fun saveInstanceState(out: Bundle) {
    }

    override fun apply(view: MainView?, retained: Boolean) {
    }

    override fun restoreInstanceState(`in`: Bundle?): RestorableViewState<MainView> {
        return this
    }
}