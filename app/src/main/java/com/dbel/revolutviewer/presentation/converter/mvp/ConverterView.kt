package com.dbel.revolutviewer.presentation.converter.mvp

import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import com.dbel.revolutviewer.presentation.core.mvp.BaseView

interface ConverterView : BaseView {

    fun onNewViewStateInstance()

    fun setRates(ratesList: List<CurrencyExchangeItem>)
    fun refreshRates(ratesList: List<CurrencyExchangeItem>)
    fun restoreRates(ratesList: List<CurrencyExchangeItem>)
    fun showContent()
    fun hideContent()
    fun moveTop(itemIndex: Int)
}