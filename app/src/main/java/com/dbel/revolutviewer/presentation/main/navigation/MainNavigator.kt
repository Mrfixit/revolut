package com.dbel.revolutviewer.presentation.main.navigation

import androidx.fragment.app.Fragment
import com.dbel.revolutviewer.presentation.converter.ui.ConverterFragment
import com.dbel.revolutviewer.presentation.core.ui.navigation.BaseNavigationActivity
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen

class MainNavigator(activity: BaseNavigationActivity<*, *, *>) :
    SupportAppNavigator(activity, activity.getContainerId()) {

    override fun createFragment(screen: SupportAppScreen?): Fragment {
        return super.createFragment(screen)
    }


    internal object Screens {
        class ConverterScreen : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return ConverterFragment.newInstance()
            }
        }
    }

}