package com.dbel.revolutviewer.presentation.core.ui.navigation.di

import com.dbel.revolutviewer.presentation.core.ui.navigation.LocalCiceroneHolder
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Provider

/**
 * Provides router by key(usually simple name of activity)
 */
class RouterProvider @Inject constructor(
    private val localCiceroneHolder: LocalCiceroneHolder,
    @Named(BaseNavigatorObjects.ROUTER_KEY) private val key: String
) : Provider<Router> {

    override fun get(): Router {
        return localCiceroneHolder.getCicerone(key).router
    }
}