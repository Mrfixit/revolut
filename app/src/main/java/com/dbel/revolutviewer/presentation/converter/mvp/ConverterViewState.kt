package com.dbel.revolutviewer.presentation.converter.mvp

import android.os.Bundle
import com.dbel.revolutviewer.presentation.converter.di.ConverterScope
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import com.dbel.revolutviewer.presentation.core.mvp.BaseViewState
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState
import javax.inject.Inject

@ConverterScope
class ConverterViewState @Inject constructor() : BaseViewState<ConverterView>() {

    private var items = arrayListOf<CurrencyExchangeItem>()
    private var state: State = State.LOADING

    override fun saveInstanceState(out: Bundle) {
        out.putSerializable(KEY_STATE, state)
        out.putParcelableArrayList(KEY_ITEMS, items)
    }

    override fun apply(view: ConverterView?, retained: Boolean) {
        when (state) {
            State.LOADING -> view?.onNewViewStateInstance()
            State.CONTENT -> view?.restoreRates(items)
        }
    }

    override fun restoreInstanceState(`in`: Bundle?): RestorableViewState<ConverterView> {
        return this
    }

    fun setContentMode(items: List<CurrencyExchangeItem>) {
        this.state = State.CONTENT
        this.items = ArrayList(items)
    }

    fun setLoadingMode() {
        this.state = State.LOADING
    }

    companion object {
        private const val KEY_STATE = "KEY_STATE"
        private const val KEY_ITEMS = "KEY_ITEMS"

        private enum class State {
            LOADING,
            CONTENT
        }
    }

}