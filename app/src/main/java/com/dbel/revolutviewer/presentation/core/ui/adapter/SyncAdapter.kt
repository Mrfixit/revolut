package com.dbel.revolutviewer.presentation.core.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dbel.revolutviewer.utils.changePosition
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager

open class SyncAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    protected val items = mutableListOf<AdapterItem>()

    private val delegatesManager = AdapterDelegatesManager<List<AdapterItem>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.onBindViewHolder(items, position, holder)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        delegatesManager.onBindViewHolder(items, position, holder, payloads)
    }

    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(items, position)
    }

    open fun addItems(newItems: List<AdapterItem>) {
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    open fun setItems(newItems: List<AdapterItem>) {
        if (items.isNotEmpty()) {
            items.clear()
        }
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    open fun silentlyRefreshItems(newItems: List<AdapterItem>) {
        val currentItems = items
        for (newItem in newItems) {
            val newItemPosition = newItems.indexOf(newItem)
            if (newItemPosition < currentItems.size) {
                replaceItem(currentItems[newItemPosition], newItem)
            } else {
                items.add(newItemPosition, newItem)
                notifyItemChanged(newItemPosition)
                notifyItemRangeChanged(newItemPosition, items.size)
            }
        }
    }

    open fun moveTop(itemIndex: Int) {
        items.changePosition(itemIndex, 0)
        notifyItemMoved(itemIndex, 0)
    }

    protected open fun replaceItem(oldItem: AdapterItem, newItem: AdapterItem) {
        if (items.contains(oldItem)) {
            val position = items.indexOf(oldItem)
            items[position] = newItem
            notifyItemChanged(position)
            notifyItemRangeChanged(position, items.size)
        }
    }


    fun addDelegates(delegates: Array<AdapterDelegate<List<AdapterItem>>>) {
        delegates.forEach { delegatesManager.addDelegate(it) }
    }

}