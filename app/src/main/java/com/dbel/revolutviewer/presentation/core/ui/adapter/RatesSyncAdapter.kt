package com.dbel.revolutviewer.presentation.core.ui.adapter

import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem

class RatesSyncAdapter : SyncAdapter() {

    override fun replaceItem(oldItem: AdapterItem, newItem: AdapterItem) {
        if (oldItem is CurrencyExchangeItem && newItem is CurrencyExchangeItem
            && oldItem.currencyCode == newItem.currencyCode
            && oldItem.isBaseCurrency == newItem.isBaseCurrency
        ) {
            refreshItem(oldItem, newItem)
        } else {
            super.replaceItem(oldItem, newItem)
        }
    }

    private fun refreshItem(oldItem: CurrencyExchangeItem, newItem: CurrencyExchangeItem) {
        notifyItemChanged(items.indexOf(oldItem), newItem.value)
    }

}