package com.dbel.revolutviewer.presentation.main.di

import com.dbel.revolutviewer.presentation.core.ui.navigation.di.BaseNavigatorObjects
import com.dbel.revolutviewer.presentation.core.ui.navigation.di.RouterProvider
import com.dbel.revolutviewer.presentation.main.MainActivity
import com.dbel.revolutviewer.presentation.main.navigation.MainNavigator
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class MainModule(activity: MainActivity) : Module() {
    init {
        bind(MainActivity::class.java).toInstance(activity)
        bind(MainNavigator::class.java).toInstance(MainNavigator(activity))
        bind(String::class.java).withName(BaseNavigatorObjects.ROUTER_KEY)
            .toInstance(activity.getSimpleName())
        bind(Router::class.java).toProvider(RouterProvider::class.java).providesSingleton()
    }
}