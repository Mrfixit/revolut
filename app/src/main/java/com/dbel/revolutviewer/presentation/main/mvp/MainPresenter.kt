package com.dbel.revolutviewer.presentation.main.mvp

import com.dbel.revolutviewer.presentation.core.mvp.BasePresenter
import com.dbel.revolutviewer.presentation.main.di.MainScope
import com.dbel.revolutviewer.presentation.main.navigation.MainNavigator
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@MainScope
class MainPresenter @Inject constructor(
    private val router: Router
) : BasePresenter<MainView>() {

    fun onFirstInit() {
        router.newRootScreen(MainNavigator.Screens.ConverterScreen())
    }

}