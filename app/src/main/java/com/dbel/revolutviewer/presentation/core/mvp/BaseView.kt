package com.dbel.revolutviewer.presentation.core.mvp

import com.hannesdorfmann.mosby3.mvp.MvpView

interface BaseView : MvpView {

    fun showError(message: String)

    fun showLoading()

    fun hideLoading()

}