package com.dbel.revolutviewer.presentation.main

import com.dbel.revolutviewer.R
import com.dbel.revolutviewer.presentation.core.ui.OnBaseCallbackListener
import com.dbel.revolutviewer.presentation.core.ui.navigation.BaseNavigationActivity
import com.dbel.revolutviewer.presentation.main.di.MainModule
import com.dbel.revolutviewer.presentation.main.di.MainScope
import com.dbel.revolutviewer.presentation.main.mvp.MainPresenter
import com.dbel.revolutviewer.presentation.main.mvp.MainView
import com.dbel.revolutviewer.presentation.main.mvp.MainViewState
import com.dbel.revolutviewer.presentation.main.navigation.MainNavigator
import ru.terrakok.cicerone.Navigator
import toothpick.config.Module
import javax.inject.Inject

class MainActivity : BaseNavigationActivity<
        MainView,
        MainPresenter,
        MainViewState>(),
    MainView, OnBaseCallbackListener {

    @Inject
    protected lateinit var injectedPresenter: MainPresenter

    @Inject
    protected lateinit var injectedViewState: MainViewState

    @Inject
    protected lateinit var injectedNavigator: MainNavigator

    override fun getContainerId(): Int = R.id.container

    override fun getLocalNavigator(): Navigator = injectedNavigator

    override fun getDeclaredModules(): Array<Module> = arrayOf(MainModule(this))

    override fun getDeclaredScope(): Class<*> = MainScope::class.java

    override fun getLayout(): Int = R.layout.activity_main

    override fun createPresenter(): MainPresenter = injectedPresenter

    override fun createViewState(): MainViewState = injectedViewState

    override fun onNewViewStateInstance() {
        getPresenter().onFirstInit()
    }

}