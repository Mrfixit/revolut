package com.dbel.revolutviewer.presentation.core.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.dbel.revolutviewer.presentation.core.mvp.BasePresenter
import com.dbel.revolutviewer.presentation.core.mvp.BaseView
import com.dbel.revolutviewer.presentation.core.mvp.BaseViewState
import com.dbel.revolutviewer.utils.di.Injection
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateFragment
import toothpick.Scope
import toothpick.Toothpick
import toothpick.config.Module

abstract class BaseFragment<C : OnBaseCallbackListener,
        V : BaseView,
        P : BasePresenter<V>,
        VS : BaseViewState<V>> : MvpViewStateFragment<V, P, VS>(), BaseView {

    protected lateinit var callbackListener: C

    private lateinit var component: Scope

    @Suppress("UNCHECKED_CAST")
    override fun onAttach(context: Context) {
        component = setupComponent()
        Toothpick.inject(this, component)
        super.onAttach(context)
        try {
            callbackListener = context as C
        } catch (exception: ClassCastException) {
            throw ClassCastException("You should have implemented ${callbackListener::class.java.simpleName} in ${context.javaClass.simpleName}")
        }
    }

    override fun onStart() {
        super.onStart()
        getPresenter().attachView(mvpView)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayout(), container, false)
    }

    override fun onStop() {
        getPresenter().detachView()
        super.onStop()
    }

    override fun onDetach() {
        Toothpick.closeScope(getDeclaredScope())
        super.onDetach()
    }

    override fun showError(message: String) {
        callbackListener.showError(message)
    }

    override fun showLoading() {
        callbackListener.showLoading()
    }

    override fun hideLoading() {
        callbackListener.hideLoading()
    }

    protected fun setupComponent(): Scope {
        val component = Toothpick.openScopes(
            Injection.ROOT_SCOPE,
            (activity as BaseActivity<*, *, *>).getDeclaredScope(),
            getDeclaredScope()
        )
        component.installModules(*getDeclaredModules())
        return component
    }


    ///////////////////////////////////////////////////////////////////////////
    // ABSTRACT DI
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Setup {@code Module}s for injection. They could use {@code PresenterScope}.
     *
     * @return Array of needed {@code Module}
     */
    protected abstract fun getDeclaredModules(): Array<Module>

    /**
     * Get component Class of the screen that should be controlled by this BaseMvpActivity.
     *
     * @return Class of component
     */
    protected abstract fun getDeclaredScope(): Class<*>

    ///////////////////////////////////////////////////////////////////////////
    // ABSTRACT UI
    ///////////////////////////////////////////////////////////////////////////

    @LayoutRes
    protected abstract fun getLayout(): Int
}