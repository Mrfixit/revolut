package com.dbel.revolutviewer.presentation.core.ui.navigation.di

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import javax.inject.Provider

class NavigationHolderProvider @Inject constructor(
    private val cicerone: Cicerone<Router>
) : Provider<NavigatorHolder> {
    override fun get(): NavigatorHolder {
        return cicerone.navigatorHolder
    }
}