package com.dbel.revolutviewer.presentation.core.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class AdapterViewHolder<in T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(item: T, position: Int = 0)

}