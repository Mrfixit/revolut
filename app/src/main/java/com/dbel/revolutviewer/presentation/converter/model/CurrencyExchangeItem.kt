package com.dbel.revolutviewer.presentation.converter.model

import com.dbel.revolutviewer.presentation.core.ui.adapter.AdapterItem
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class CurrencyExchangeItem(
    val currencyCode: String,
    var value: BigDecimal,
    var isBaseCurrency: Boolean = false
) : AdapterItem() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CurrencyExchangeItem

        if (currencyCode != other.currencyCode) return false
        if (isBaseCurrency != other.isBaseCurrency) return false

        return true
    }

    override fun hashCode(): Int {
        var result = currencyCode.hashCode()
        result = 31 * result + isBaseCurrency.hashCode()
        return result
    }
}