package com.dbel.revolutviewer.presentation.core.ui.adapter

import android.os.Parcelable
import com.dbel.revolutviewer.presentation.core.vo.BaseVo
import java.util.*

abstract class AdapterItem(
    private val privateId: String = UUID.randomUUID().toString()
) : Parcelable, BaseVo()