package com.dbel.revolutviewer.presentation.converter.model

/**
 * Search element in currency list by currency code, if not exists returns null
 */
fun List<CurrencyExchangeItem>.findItemByCurrencyCode(currencyCode: String): CurrencyExchangeItem? {

    for (item in this) {
        if (item.currencyCode == currencyCode) {
            return item
        }
    }

    return null
}