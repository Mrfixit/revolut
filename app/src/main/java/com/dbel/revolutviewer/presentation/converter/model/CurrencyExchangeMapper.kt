package com.dbel.revolutviewer.presentation.converter.model

import com.dbel.revolutviewer.domain.models.CurrencyRatesDataLo
import java.math.BigDecimal

fun CurrencyRatesDataLo.toAdapterItems(inputAmount: BigDecimal): List<CurrencyExchangeItem> {

    val result = mutableListOf<CurrencyExchangeItem>()
    for ((key, value) in this.rates) {
        result.add(CurrencyExchangeItem(key, value.multiply(inputAmount)))
    }

    return result
}