package com.dbel.revolutviewer.presentation.converter.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.dbel.revolutviewer.R
import com.dbel.revolutviewer.presentation.converter.di.ConverterModule
import com.dbel.revolutviewer.presentation.converter.di.ConverterScope
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import com.dbel.revolutviewer.presentation.converter.mvp.ConverterPresenter
import com.dbel.revolutviewer.presentation.converter.mvp.ConverterView
import com.dbel.revolutviewer.presentation.converter.mvp.ConverterViewState
import com.dbel.revolutviewer.presentation.converter.ui.adapter.ConverterCurrencyRowDelegate
import com.dbel.revolutviewer.presentation.core.ui.BaseActivity
import com.dbel.revolutviewer.presentation.core.ui.BaseFragment
import com.dbel.revolutviewer.presentation.core.ui.OnBaseCallbackListener
import com.dbel.revolutviewer.presentation.core.ui.adapter.RatesSyncAdapter
import com.dbel.revolutviewer.presentation.core.ui.adapter.VerticalLinearSpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_converter.*
import toothpick.config.Module
import javax.inject.Inject

class ConverterFragment : BaseFragment<
        OnBaseCallbackListener,
        ConverterView,
        ConverterPresenter,
        ConverterViewState>(), ConverterView {

    @Inject
    protected lateinit var injectetPresenter: ConverterPresenter

    @Inject
    protected lateinit var injectViewState: ConverterViewState

    private lateinit var adapter: RatesSyncAdapter
    override fun getDeclaredModules(): Array<Module> = arrayOf(ConverterModule(this))

    override fun getDeclaredScope(): Class<*> = ConverterScope::class.java

    override fun getLayout(): Int = R.layout.fragment_converter

    override fun createPresenter(): ConverterPresenter = injectetPresenter

    override fun createViewState(): ConverterViewState = injectViewState

    override fun onNewViewStateInstance() {
        getPresenter().onFirstInit()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    override fun showContent() {
        recycler_converter.visibility = View.VISIBLE
    }

    override fun hideContent() {
        recycler_converter.visibility = View.INVISIBLE
    }

    override fun showLoading() {
        progress_converter.visibility = View.VISIBLE
        getViewState()?.setLoadingMode()
    }

    override fun hideLoading() {
        progress_converter.visibility = View.INVISIBLE
    }

    override fun setRates(ratesList: List<CurrencyExchangeItem>) {
        adapter.setItems(ratesList)
        getViewState()?.setContentMode(ratesList)
    }

    override fun refreshRates(ratesList: List<CurrencyExchangeItem>) {
        adapter.silentlyRefreshItems(ratesList)
        getViewState()?.setContentMode(ratesList)
    }

    override fun restoreRates(ratesList: List<CurrencyExchangeItem>) {
        adapter.setItems(ratesList)
        getPresenter().onRestoreData()
        getViewState()?.setContentMode(ratesList)
    }

    override fun moveTop(itemIndex: Int) {
        adapter.moveTop(itemIndex)
        recycler_converter.scrollToPosition(0)
    }

    private fun initUi() {
        toolbar_converter.setTitle(R.string.app_name)
        (activity as BaseActivity<*, *, *>).setSupportActionBar(toolbar_converter)

        val marginItems = resources.getDimensionPixelOffset(R.dimen.margin_converter_items)
        val marginTop = resources.getDimensionPixelOffset(R.dimen.margin_converter_list_top)
        val marginBottom = resources.getDimensionPixelOffset(R.dimen.margin_converter_list_bottom)

        adapter = RatesSyncAdapter()
        adapter.addDelegates(
            arrayOf(
                ConverterCurrencyRowDelegate(
                    { getPresenter().onRateItemClick(it) },
                    { getPresenter().onBaseItemAmountChange(it) })
            )
        )
        recycler_converter.adapter = adapter

        recycler_converter.setHasFixedSize(true)

        recycler_converter.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        recycler_converter.addItemDecoration(
            VerticalLinearSpaceItemDecoration(
                marginItems,
                marginTop,
                marginBottom
            )
        )
    }

    companion object {
        fun newInstance(): Fragment {
            return ConverterFragment()
        }
    }
}