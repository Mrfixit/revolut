package com.dbel.revolutviewer.presentation.main.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope {
}