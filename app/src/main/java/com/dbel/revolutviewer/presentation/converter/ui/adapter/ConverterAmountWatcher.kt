package com.dbel.revolutviewer.presentation.converter.ui.adapter

import android.text.Editable
import android.text.TextWatcher
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import java.math.BigDecimal

/**
 * Custom watcher class to avoid problems with wrong values from others recycler view items
 */
class ConverterAmountWatcher(
    private val textChangeListener: (item: CurrencyExchangeItem) -> Unit
) : TextWatcher {

    lateinit var item: CurrencyExchangeItem

    override fun afterTextChanged(newAmount: Editable?) {
        if (item.isBaseCurrency) {
            item.value = if (newAmount != null && newAmount.isNotEmpty()) {
                BigDecimal(newAmount.toString())
            } else {
                BigDecimal.ZERO
            }
            textChangeListener(item)
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        ///
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        ///
    }

}