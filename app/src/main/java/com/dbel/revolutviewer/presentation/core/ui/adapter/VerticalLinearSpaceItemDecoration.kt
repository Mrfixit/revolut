package com.dbel.revolutviewer.presentation.core.ui.adapter

import android.graphics.Rect
import android.view.View
import androidx.annotation.Px
import androidx.recyclerview.widget.RecyclerView

class VerticalLinearSpaceItemDecoration(
    @Px private val marginBetweenItems: Int = 0,
    @Px private val topMargin: Int = 0,
    @Px private val bottomMargin: Int = 0
) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        setForHorizontalScroll(outRect, view, parent, state)
    }

    private fun setForHorizontalScroll(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State?
    ) {
        val position = parent.getChildAdapterPosition(view)
        val isLast = position == state!!.itemCount - 1
        if (isLast) {
            outRect.bottom = bottomMargin
            outRect.top = marginBetweenItems //don't forget about recycling...
        } else if (position == 0) {
            outRect.top = topMargin
            // don't recycle bottom if first item is also last
            // should keep bottom padding set above
            if (!isLast)
                outRect.bottom = marginBetweenItems
        } else {
            outRect.top = marginBetweenItems
            outRect.bottom = marginBetweenItems
        }
    }
}