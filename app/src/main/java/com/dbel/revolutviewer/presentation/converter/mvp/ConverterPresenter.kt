package com.dbel.revolutviewer.presentation.converter.mvp

import com.dbel.revolutviewer.domain.usecases.GetRatesForCurrencyEachSecondUseCase
import com.dbel.revolutviewer.presentation.converter.di.ConverterScope
import com.dbel.revolutviewer.presentation.converter.model.CurrencyExchangeItem
import com.dbel.revolutviewer.presentation.converter.model.findItemByCurrencyCode
import com.dbel.revolutviewer.presentation.converter.model.toAdapterItems
import com.dbel.revolutviewer.presentation.core.mvp.BasePresenter
import com.dbel.revolutviewer.utils.changePosition
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

@ConverterScope
class ConverterPresenter @Inject constructor(
    private val getRatesUseCase: GetRatesForCurrencyEachSecondUseCase
) : BasePresenter<ConverterView>() {

    private var inputAmount = DEFAULT_AMOUNT
    private var inputCurrency = DEFAULT_CURRENCY
    private var currentRates = LinkedList<CurrencyExchangeItem>()

    fun onFirstInit() {

        ifViewAttached { view ->
            view.hideContent()
            view.showLoading()
        }

        subscribeForData()
    }

    fun onRestoreData() {
        subscribeForData()
    }

    fun onRateItemClick(item: CurrencyExchangeItem) {
        makeBaseCurrency(item)
    }

    fun onBaseItemAmountChange(baseItem: CurrencyExchangeItem) {
        if (inputCurrency == baseItem.currencyCode && this.inputAmount != baseItem.value) {
            this.inputAmount = baseItem.value
        }
    }

    private fun subscribeForData() {
        add(getRatesUseCase.execute(inputCurrency)
            .doOnNext {
                ifViewAttached { view ->
                    view.hideLoading()
                    view.showContent()
                }
            }
            .map { it.toAdapterItems(inputAmount) }
            .map {
                val result = mutableListOf(CurrencyExchangeItem(inputCurrency, inputAmount, true))
                result.addAll(it)
                return@map result
            }
            .subscribe({
                invalidateData(it)
            }, {
                invalidateData(currentRates)
                ifViewAttached { view -> view.showError("Something went wrong") }
            })
        )
    }

    @Synchronized
    private fun invalidateData(newRates: List<CurrencyExchangeItem>) {
        if (currentRates.isEmpty()) {
            this.currentRates = LinkedList(newRates)
            this.inputCurrency = currentRates[0].currencyCode
            this.inputAmount = currentRates[0].value
            ifViewAttached { view -> view.setRates(currentRates) }
        } else {
            refreshRates(currentRates, newRates)
            this.inputCurrency = currentRates[0].currencyCode
            this.inputAmount = currentRates[0].value
            ifViewAttached { view -> view.refreshRates(currentRates) }
        }
    }

    private fun refreshRates(
        currentRates: LinkedList<CurrencyExchangeItem>,
        newRates: List<CurrencyExchangeItem>
    ) {
        val absolutelyNewItems = arrayListOf<CurrencyExchangeItem>()
        for (newRate in newRates) {
            val currentRate = currentRates.findItemByCurrencyCode(newRate.currencyCode)
            if (currentRate != null) {
                currentRates[currentRates.indexOf(currentRate)] = newRate
            } else {
                absolutelyNewItems.add(newRate)
            }
        }

        if (absolutelyNewItems.isNotEmpty()) {
            currentRates.addAll(absolutelyNewItems)
        }
    }

    @Synchronized
    private fun makeBaseCurrency(newBaseItem: CurrencyExchangeItem) {

        val oldBaseItemIndex = 0
        val newBaseItemIndex = currentRates.indexOf(newBaseItem)

        if (newBaseItemIndex >= 0) {
            clearSubscriptions()
            currentRates[oldBaseItemIndex].isBaseCurrency = false
            currentRates[newBaseItemIndex].isBaseCurrency = true
            animateMovingTop(newBaseItemIndex)
            invalidateData(currentRates)
            subscribeForData()
        }
    }

    private fun animateMovingTop(newBaseItemIndex: Int) {
        currentRates.changePosition(newBaseItemIndex, 0)
        ifViewAttached { view -> view.moveTop(newBaseItemIndex) }
    }

    companion object {
        private val DEFAULT_AMOUNT = BigDecimal(100)
        private const val DEFAULT_CURRENCY = "EUR"
    }

}