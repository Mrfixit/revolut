package com.dbel.revolutviewer.presentation.core.di

import com.dbel.revolutviewer.presentation.core.ui.navigation.LocalCiceroneHolder
import com.dbel.revolutviewer.presentation.core.ui.navigation.di.NavigationHolderProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import toothpick.config.Module

class PresentationModule : Module() {

    init {
        bind(Cicerone::class.java).toInstance(Cicerone.create())
        bind(NavigatorHolder::class.java)
            .toProvider(NavigationHolderProvider::class.java)
            .providesSingleton()
        bind(LocalCiceroneHolder::class.java).toInstance(LocalCiceroneHolder())
    }

}