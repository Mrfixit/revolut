package com.dbel.revolutviewer.presentation.core.ui.navigation.di


object BaseNavigatorObjects {
    /**
     * Name of the holder of local holders names
     */
    const val ROUTER_KEY = "ROUTER_KEY"
}