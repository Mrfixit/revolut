package com.dbel.revolutviewer.utils.app

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.dbel.revolutviewer.utils.di.Injection
import com.jakewharton.threetenabp.AndroidThreeTen

class Application : MultiDexApplication() {
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)

    }

    override fun onCreate() {
        super.onCreate()

        Injection.init(this)

        setupTime()
    }


    private fun setupTime() {
        AndroidThreeTen.init(this)
    }


}