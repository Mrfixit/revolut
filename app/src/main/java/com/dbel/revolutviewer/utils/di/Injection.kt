package com.dbel.revolutviewer.utils.di

import android.app.Application
import com.dbel.revolutviewer.data.core.di.DataModule
import com.dbel.revolutviewer.domain.core.di.DomainModule
import com.dbel.revolutviewer.presentation.core.di.PresentationModule
import com.dbel.revolutviewer.utils.app.ApplicationModule
import toothpick.Toothpick
import toothpick.config.Module
import toothpick.configuration.Configuration

object Injection {

    const val ROOT_SCOPE = "ROOT_SCOPE"


    fun init(application: Application) {
        setupToothpick()
        setupRootScope(application)
    }

    fun clearOpenScopes() {
        Toothpick.closeScope(BaseScope::class.java)
    }

    fun reset() {
        Toothpick.reset()
    }


    private fun setupToothpick() {
        Toothpick.setConfiguration(
            Configuration.forProduction()
                .preventMultipleRootScopes()
        )

    }

    private fun setupRootScope(application: Application) {
        val rootScope = Toothpick.openScope(ROOT_SCOPE)
        rootScope.installModules(*getRootModules(application))
    }


    internal fun getRootModules(application: Application): Array<Module> {
        return arrayOf(
            ApplicationModule(application),
            DataModule(),
            DomainModule(),
            PresentationModule()
        )
    }


}