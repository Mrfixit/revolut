package com.dbel.revolutviewer.utils

import java.math.BigInteger
import java.security.MessageDigest

/**
 * Converts any string to md5 hash
 */
fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}