package com.dbel.revolutviewer.utils

import java.util.*

fun <T : Any> MutableList<T>.changePosition(from: Int, to: Int) {
    val item = this[from]
    this.remove(item).also { add(to, item) }
}

fun <T : Any> LinkedList<T>.changePosition(from: Int, to: Int) {
    val item = this[from]
    this.remove(item).also { add(to, item) }
}
