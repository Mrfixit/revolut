package com.dbel.revolutviewer.utils.schedulers

import io.reactivex.Scheduler

interface SchedulerManager {
    fun getIoScheduler(): Scheduler

    fun getMainScheduler(): Scheduler
}