package com.dbel.revolutviewer.utils.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerManagerImpl : SchedulerManager {

    override fun getIoScheduler(): Scheduler = Schedulers.io()

    override fun getMainScheduler(): Scheduler = AndroidSchedulers.mainThread()

}