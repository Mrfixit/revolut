package com.dbel.revolutviewer.utils.app

import android.app.Application
import android.content.Context
import com.dbel.revolutviewer.utils.schedulers.SchedulerManager
import com.dbel.revolutviewer.utils.schedulers.SchedulerManagerImpl
import toothpick.config.Module

//Application level Module(aka application scope)
class ApplicationModule(application: Application) : Module() {

    init {
        bind(Application::class.java).toInstance(application)
        bind(Context::class.java).toInstance(application)
        bind(SchedulerManager::class.java).toInstance(SchedulerManagerImpl())
    }

}