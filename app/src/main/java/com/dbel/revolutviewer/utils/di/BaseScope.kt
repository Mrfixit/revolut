package com.dbel.revolutviewer.utils.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseScope