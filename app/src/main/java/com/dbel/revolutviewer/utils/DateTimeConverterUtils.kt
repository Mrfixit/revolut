/*
 * Copyright © 2017. Vivira Health Lab. All rights reserved.
 */

package com.dbel.revolutviewer.utils

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

/**
 * Helper class to convert from [Date] to java.time backport ThreeTenABP classes.
 */
object DateTimeConverterUtils {

    private val DATE_FORMAT = "yyyy-MM-dd"
    private val DATE_REAL_FORMAT = "EEEE, d.M.yy"
    private val DATE_REAL_FORMAT_US = "EEEE, M.d.yy"
    private val DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"

    /**
     * Converts date string in yyyy-MM-dd format to [LocalDate].
     *
     * @param dateString date as yyyy-MM-dd
     * @return [LocalDate]
     */
    fun asLocalDate(dateString: String): LocalDate {
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        return LocalDate.parse(dateString, formatter)
    }

    /**
     * Converts [LocalDate] to date string in yyyy-MM-dd format.
     *
     * @param localDate [LocalDate]
     * @return date of yyyy-MM-dd format
     */
    fun asString(localDate: LocalDate): String {
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        return formatter.format(localDate)
    }

}
